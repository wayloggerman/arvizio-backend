import express, { Request, Response, Express, json, response } from 'express'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import morgan from 'morgan'
import Signup from './src/routes/signup'
import Login from './src/routes/login'
import cfg from './cfg'
import auth from './src/middleware/auth'
import Auth from './src/routes/auth'
import Logout from './src/routes/logout'
import Confirm from './src/routes/confirm'
import newPassword from './src/routes/newPassword'
import passport, { use } from 'passport'
import VKStrategy from 'passport-vkontakte'
import Yandex from 'passport-yandex'
import GoogleStrategy from 'passport-google-oauth20'
import db from './src/db/db'
import User from './src/entity/User'
import UserRoute from './src/routes/user'
import Session from './src/entity/Session'
import Sessions from './src/routes/sessions'
import Projects from './src/routes/projects'
import fileUpload from 'express-fileupload'
import path from 'path'
import DataLoader from './src/routes/data'
import https from 'https'
require('module-alias/register')

class ArvizioBackend {
    private authMiddleware() {
        passport.use(
            new Yandex.Strategy(
                {
                    clientID: '2e3060086a8d4b209fce595c00801123',
                    clientSecret: '92aff19be60c4641887730546be3ee81',
                    callbackURL:
                        'http://arviziotest.ru:3333/auth/yandex/callback',
                },
                (accessToken, refreshToken, profile, done) => {
                    done(null, profile)
                }
            )
        ),
            passport.use(
                new VKStrategy.Strategy(
                    {
                        clientID: '8183066',
                        clientSecret: 'NCAIZ6ENaJ71npXquXEx',
                        callbackURL:
                            'http://arviziotest.ru:3333/auth/vkontakte/callback',
                        profileFields: ['email', 'city', 'bdate'],
                    },
                    (accessToken, refreshToken, params, profile, cb) => {
                        cb(null, profile)
                    }
                )
            )

        const googleParams = {
            clientID:
                '135760258613-shk0oa7sgjkjfcu4sllit5pflfk2n6bb.apps.googleusercontent.com',
            clientSecret: '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',
            callbackURL: 'http://arviziotest.ru:3333/auth/google/callback',
        }

        const googleStrategy = new GoogleStrategy.Strategy(
            googleParams,
            (accessToken, refreshToken, profile, cb) => {
                cb(null, profile)
            }
        )

        passport.use(googleStrategy)

        this.app.use(passport.initialize())
        this.app.get(
            '/auth/vkontakte',
            passport.authenticate('vkontakte', {
                session: false,
            })
        )

        this.app.get(
            '/auth/yandex',
            passport.authenticate('yandex', {
                session: false,
            })
        )

        this.app.get(
            '/auth/vkontakte/callback',
            passport.authenticate('vkontakte', {
                session: false,
            }),
            async (request: Request, response: Response) => {
                if (!request.user) response.redirect(cfg.url.frontend as string)

                const { username } = request.user as any
                const { id } = request.user as any
                const photoUrl = (request.user as any).photos[0]

                let userNote = await User.findByMail(username)

                if (!userNote || !userNote.id) {
                    userNote = await User.passportUser(
                        username,
                        `vk|${id}`,
                        photoUrl.value
                    )
                }

                const token = await Session.create(userNote.id)

                response.cookie('token', token, cfg.cookieOpt)

                response.redirect(cfg.url.frontend as string)
            }
        )

        this.app.get(
            '/auth/google',
            passport.authenticate('google', {
                scope: ['profile', 'email'],
                session: false,
            })
        )

        this.app.get(
            '/auth/google/callback',
            passport.authenticate('google', {
                session: false,
            }),
            async (request: Request, response: Response) => {
                const { user } = request

                console.log(JSON.stringify(user, null, 1))

                if (!user) return response.redirect(cfg.url.frontend as string)

                const { emails } = user as {
                    emails: { value: string; verified: boolean }[]
                }
                const { id } = user as { id: string }
                const { photos } = user as { photos: { value: string }[] }

                if (!emails || !emails.length)
                    return response.redirect(
                        `${cfg.url.frontend}/?auth=false` as string
                    )

                // find user

                let userNote = await User.findByMail(emails[0].value)

                if (!userNote || !userNote.id) {
                    userNote = await User.passportUser(
                        emails[0].value,
                        `google|${id}`,
                        photos[0].value
                    )
                }

                const token = await Session.create(userNote.id)

                response.cookie('token', token, cfg.cookieOpt)

                response.redirect(cfg.url.frontend as string)
            }
        )
        this.app.get(
            '/auth/yandex/callback',
            passport.authenticate('yandex', {
                session: false,
            }),
            async (request: Request, response: Response) => {
                const { user } = request

                if (!user) return response.redirect(cfg.url.frontend as string)

                const { emails } = user as {
                    emails: { value: string; verified: boolean }[]
                }
                const { id } = user as { id: string }
                const { photos } = user as { photos: { value: string }[] }

                if (!emails || !emails.length)
                    return response.redirect(
                        `${cfg.url.frontend}/?auth=false` as string
                    )

                // find user

                let userNote = await User.findByMail(emails[0].value)

                if (!userNote || !userNote.id) {
                    userNote = await User.passportUser(
                        emails[0].value,
                        `google|${id}`,
                        photos[0].value
                    )
                }

                const token = await Session.create(userNote.id)

                response.cookie('token', token, cfg.cookieOpt)

                response.redirect(cfg.url.frontend as string)
            }
        )
    }

    private middleware() {
        this.app.use(json())
        this.app.use(
            fileUpload({
                limits: { fileSize: 50 * 1024 * 1024 * 1024 },
            })
        )
        this.app.use(
            cors({
                credentials: true,
                origin: [
                    'http://192.167.1.220:8000',
                    'http://192.167.1.220:5000',
                    'http://localhost:5000',
                    'http://localhost:5003',
                    'http://arviziotest.ru',
                    'http://185.251.88.184',
                    'https://arvizio.samaritanit.ru',
                ],
            })
        )

        this.app.use(cookieParser())
        this.app.use(
            express.static(
                process.env.STORAGE || path.join(__dirname, 'public')
            )
        )

        this.app.use(
            cfg.IS_DEV
                ? morgan(function (tokens, req, res) {
                      return [
                          tokens.method(req, res),
                          tokens.url(req, res),
                          JSON.stringify(req.body),
                          tokens.status(req, res),
                          tokens.res(req, res, 'content-length'),
                          '-',
                          tokens['response-time'](req, res),
                          'ms',
                      ].join(' ')
                  })
                : morgan('combined')
        )
    }
    private listen() {
        this.server.listen(this.port, () => {
            console.log(`arvizio backend start on ${this.port} port`)
        })
    }

    private routes() {
        this.app.get('/', (req, res) => {
            res.send('Hello World!')
        })
        this.app.use(Login.router.path, Login.router.handler())
        this.app.use(Signup.router.path, Signup.router.handler())
        this.app.use(Auth.router.path, Auth.router.handler())
        this.app.use(Logout.router.path, auth(), Logout.router.handler())
        this.app.use(Confirm.router.path, Confirm.router.handler())
        this.app.use(newPassword.router.path, newPassword.router.handler())
        this.app.use(UserRoute.router.path, auth(), UserRoute.router.handler())
        this.app.use(Sessions.router.path, Sessions.router.handler())
        this.app.use(Projects.router.path, auth(), Projects.router.handler())
        this.app.use(
            DataLoader.router.path,
            auth(),
            DataLoader.router.handler()
        )
    }

    private server
    constructor(private app: Express, private port: string | undefined) {
        this.middleware()
        this.authMiddleware()
        this.routes()

        this.server = cfg.IS_DEV
            ? this.app
            : https.createServer(
                  {
                      key: cfg.ssl.key,
                      cert: cfg.ssl.cert,
                      ca: cfg.ssl.ca,
                  },
                  this.app
              )
        this.listen()
    }
}

process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason)
})
process.on('uncaughtException', (err) => {
    console.log('Uncaught Exception thrown', err)
})

new ArvizioBackend(express(), process.env.PORT)
