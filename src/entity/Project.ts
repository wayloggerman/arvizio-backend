import _ from 'lodash';
import db from '../db/db';
import { defaultSettings, IProjectSchema, projectColumns, ProjectStorage, ProjectType } from '../interface';
import Session from './Session';


export default class Project {

    static table: string = 'projects'


    private constructor() {
    }

    static create(opt: { name: string, type: ProjectType, parentSite: string, parentSiteText: string, token: string, option: string }) {
        return new Project().create({
            name: opt.name,
            parentSite: opt.parentSite,
            type: opt.type,
            token: opt.token,
            textParetnSite: opt.parentSiteText,
            option: opt.option,
        })
    }

    // update

    static update(id: number, userId: number, x: IProjectSchema): Promise<IProjectSchema[]> {
        return new Project().update(id, userId, x)
    }

    async update(id: number, userId: number, x: IProjectSchema): Promise<IProjectSchema[]> {
        return db(Project.table).update(x).where({ id, [projectColumns.owner_id]: userId }).returning('*')
    }

    // rename
    async rename(id: number, userId: number, name: string): Promise<IProjectSchema[]> {
        return this.update(id, userId, { name })
    }

    // logo
    async setLogo(id: number, userId: number): Promise<IProjectSchema[]> {
        const logoLink = `logo`
        return this.update(id, userId, { logo_link: logoLink })
    }

    static setLogo(id: number, userId: number): Promise<IProjectSchema[]> {
        return new Project().setLogo(id, userId)
    }




    // create
    async create(opt: {
        name: string,
        type: ProjectType,
        parentSite: string,
        textParetnSite: string,
        token: string,
        option: string

    }) {

        const userId = await db(Session.table)
            .select('user_id')
            .where({ session_id: opt.token })
            .first().then(res => res.user_id)

        return db(Project.table).insert({
            [projectColumns.name]: opt.name,
            [projectColumns.type]: opt.type,
            [projectColumns.settings]: {
                ...defaultSettings,
                options: opt.option ?? null,
                parentSite: opt.parentSite,
                textParetnSite: opt.textParetnSite,

            },
            [projectColumns.owner_id]: userId,
            [projectColumns.storage]: ProjectStorage.local,
            [projectColumns.created_at]: db.raw('now()'),



        }).returning('id')
    }



    // read
    async getAll(userId: number): Promise<IProjectSchema[]> {
        return db(Project.table).select('*').where({ owner_id: userId })
    }

    async get(id: number, userId: number): Promise<IProjectSchema> {

        return db(Project.table).select('*').where({ id, [projectColumns.owner_id]: userId }).first()
    }

    static getAll(userId: number): Promise<IProjectSchema[]> {
        return new Project().getAll(userId)
    }

    static get(id: number, userId: number): Promise<IProjectSchema> {
        if (_.isNaN(id)) {
            throw new Error('id must be a number')
        }
        return new Project().get(
            id
            , userId)
    }


}
