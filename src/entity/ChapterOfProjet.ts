import db from '../db/db'


interface ChapterSettings {
    prefix: string;
    picCount: number;
}

export default class ChapterOfProjects {
    private columns = {
        id: 'id',
        project_id: 'project_id',
        settings: 'settings',
        deleted: 'deleted',
        deleted_at: 'deleted_at',
    }

    private table = 'chapters_of_projects'

    private constructor() { }

    static create(opt: {
        projectId: number,
        settings: any,
    }) {
        return new ChapterOfProjects().create({
            projectId: opt.projectId,
            settings: opt.settings,
        })
    }

    async create(opt: {
        projectId: number,
        settings: any,
    }) {
        return db(this.table).insert({
            project_id: opt.projectId,
            settings: opt.settings,
        }).returning('*')
    }

    // update
    async update(id: number, { settings }: { settings: any }) {
        return db(this.table).update({
            settings,
        }).where({ id }).returning('*')
    }

    // delete
    async delete(id: number) {
        return db(this.table).update({
            deleted: true,
            deleted_at: db.fn.now(),
        }).where({ id }).returning('*')
    }

    // get

    async get(id: number) {
        return db(this.table).select('*').where({ id }).first()
    }
}
