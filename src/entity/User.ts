import db from '../db/db'


export default class User {
    router() {
        throw new Error('Method not implemented.')
    }

    table = 'users'


    constructor(private cols: { email?: string, password?: string, sourse?: string, confirmToken?: string, avatar?: string, id?: number }) {

    }

    async find() {
        return db(this.table).select('*').where({ email: this.cols.email, password: db.raw("crypt(?, password)", [this.cols.password as string]) })
    }
    async findById() {
        return db(this.table).select('*').where({ id: this.cols.id })
    }

    static find(email: string, password: string) {
        return new User({ email, password }).find()
    }

    static findById(id: number) {
        return new User({ id }).findById()
    }

    async findByMail(): Promise<{ id: number }> {
        return db(this.table).select('*').where({ email: this.cols.email }).then(res => res[0])
    }

    static findByMail(email: string) {
        return new User({ email }).findByMail()
    }




    async create() {
        return db(this.table).insert({
            email: this.cols.email,
            password: db.raw(`crypt(?, gen_salt('md5'))`, [this.cols.password as string]),
            created_at: db.raw('now()'),
            is_confirmed: false,
            confirm_token: this.cols.confirmToken,
        })
    }
    static create(email: string, password: string, confirmToken: string) {
        return new User({ email, password, confirmToken }).create()
    }


    private async updatePassword() {
        return db(this.table).update({
            password: db.raw(`crypt(?, gen_salt('md5'))`, [this.cols.password as string]),
        }).where({ email: this.cols.email })
    }

    static updatePassword(email: string, password: string) {
        return new User({ email, password }).updatePassword()
    }


    private async confirmUser() {
        return db(this.table).update({ is_confirmed: true }).where({
            confirm_token: this.cols.confirmToken, is_confirmed: false
        }).returning('id')
    }

    static confirmUser(confirmToken: string) {
        return new User({ confirmToken, email: '', password: '' }).confirmUser()
    }


    private async refreshConfirm() {
        return db(this.table).update({
            confirm_token: this.cols.confirmToken
        }).where({ email: this.cols.email })
    }

    static refreshConfirm(email: string, confirmToken: string) {
        return new User({ confirmToken, email }).refreshConfirm()
    }


    private async passportUser(): Promise<{ id: number }> {
        return db(this.table).insert({
            email: this.cols.email,
            source_external_id: this.cols.sourse,
            created_at: db.raw('now()'),
            avatar: this.cols.avatar
        }).returning('id').then(res => res[0])
    }

    static passportUser(email: string, sourse: string, avatar: string) {
        return new User({ email, sourse, avatar }).passportUser()
    }



}
