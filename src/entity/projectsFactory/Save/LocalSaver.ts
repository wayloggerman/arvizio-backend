import cfg from '@root/cfg'
import {
    GroupFiles,
    IProjectSchema,
    UploadProjectFile,
} from '@root/src/interface'
import fs from 'fs/promises'
import _ from 'lodash'
import Project from '../../Project'
import SaverLoader from './Saver'

export default class LocalSaver extends SaverLoader {
    private projectPath: string

    private async prepareProjectPath(
        path: string = this.projectPath
    ): Promise<void> {
        try {
            await fs.mkdir(path, { recursive: true })
        } catch (e) {
            console.error(e)
            // ignore if folder already exists
        }
    }

    saveVideo(file: UploadProjectFile): Promise<boolean> {
        throw new Error('Method not implemented.')
    }
    async saveLogo(file: UploadProjectFile): Promise<boolean> {
        try {
            await this.prepareProjectPath()

            const { buffer } = file

            const path = `${this.projectPath}/logo.${file.ext}`

            this.project.settings?.files.push({
                name: 'logo',
                type: file.type,
                mime: file.mime,
                path: `${this.project.link_to_change}/logo.${file.ext}`,
                spots: [],
            })

            if (this.project.id && this.project.owner_id) {
                await Project.update(
                    this.project.id,
                    this.project.owner_id,
                    this.project
                )
            }
            const res = await fs.writeFile(path, buffer)
            console.log({ saveLogo: res })

            return true
        } catch (e) {
            console.error(e)
            return false
        }
    }
    async saveImage(file: UploadProjectFile): Promise<boolean> {
        try {
            if (!this.project.settings) return false
            if (!this.project.owner_id) return false
            if (!this.project.id) return false

            await this.prepareProjectPath()

            const { settings } = this.project

            const { buffer } = file

            const name = `pic-${settings.imageCounter++}.${file.ext}`

            settings.files.push({
                name,
                type: file.type,
                mime: file.mime,
                path: `${this.project.link_to_change}/${name}`,
                spots: [],
            })

            await Project.update(
                this.project.id,
                this.project.owner_id,
                this.project
            )

            const path = `${this.projectPath}/${name}`

            const res = await fs.writeFile(path, buffer)

            console.log({ saveImage: res })
            return true
        } catch (e) {
            console.error(e)
            return false
        }
    }
    async saveImageGroup(file: UploadProjectFile): Promise<boolean> {
        try {
            if (!this.project.settings) return false
            if (!this.project.owner_id) return false
            if (!this.project.id) return false
            if (_.isNil(file.group)) return false

            const { settings } = this.project
            const { groupFiles } = settings

            // если группа не найдена, то создаем новую
            if (file.group === groupFiles.length)
                settings.groupFiles.push({
                    groupKey: file.group.toString(),
                    count: 0,
                    files: [],
                })

            // получаем нужную группу
            const group = settings.groupFiles[file.group]

            const name = `${file.name}`

            // сохраняем файл
            group.files.push({
                name,
                type: file.type,
                mime: file.mime,
                path: `${this.project.link_to_change}/${group.groupKey}/${name}`,
                spots: [],
            })

            await Project.update(
                this.project.id,
                this.project.owner_id,
                this.project
            )

            this.projectPath = `${cfg.uploadsDir}/${this.project.link_to_change}/${file.group}`
            await this.prepareProjectPath()

            const path = `${this.projectPath}/${name}`

            await fs.writeFile(path, file.buffer)

            return true
        } catch (e) {
            console.error(e)
            return false
        }
    }
    async saveAudio(file: UploadProjectFile): Promise<boolean> {
        try {
            if (!this.project.settings) return false
            if (!this.project.owner_id) return false
            if (!this.project.id) return false

            const { settings } = this.project
            const { groupFiles } = settings

            // получаем нужную группу

            const group = groupFiles[groupFiles.length - 1]

            const name = `audio.${file.ext}`

            // сохраняем файл
            group.files.push({
                name,
                type: file.type,
                mime: file.mime,
                path: `${this.project.link_to_change}/${group.groupKey}/${name}`,
                spots: [],
            })

            await Project.update(
                this.project.id,
                this.project.owner_id,
                this.project
            )

            this.projectPath = `${cfg.uploadsDir}/${this.project.link_to_change}/${group.groupKey}`
            await this.prepareProjectPath()

            const path = `${this.projectPath}/${name}`

            await fs.writeFile(path, file.buffer)

            return true
        } catch (e) {
            console.error(e)
            return false
        }
    }

    constructor(project: IProjectSchema) {
        super(project)

        this.projectPath = `${cfg.uploadsDir}/${project.link_to_change}`
    }
}
