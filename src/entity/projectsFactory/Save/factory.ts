import { FileType, IProjectSchema, ProjectStorage } from '@root/src/interface'
import fileUpload from 'express-fileupload'
import LocalSaver from './LocalSaver'
import Saver from './Saver'

class SaverFactory {
    static create(project: IProjectSchema): Saver {
        switch (project.storage) {
            case ProjectStorage.local:
                return new LocalSaver(project)
            default:
                throw new Error('Unknown storage type')
        }
    }
}

export default function save(
    project: IProjectSchema,
    files: fileUpload.FileArray,
    type: FileType,
    saveOpt: any = undefined
) {
    const saver = SaverFactory.create(project)
    return saver.save(files, type, saveOpt)
}
