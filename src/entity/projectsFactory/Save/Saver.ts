import {
    FileType,
    IProjectSchema,
    ProjectFile,
    ProjectType,
    UploadProjectFile,
} from '@root/src/interface'
import fileUpload, { UploadedFile } from 'express-fileupload'
import _ from 'lodash'

export default abstract class Saver {
    constructor(protected project: IProjectSchema) {}

    protected files: UploadProjectFile[] = []

    abstract saveLogo(file: UploadProjectFile): Promise<boolean>
    abstract saveImage(file: UploadProjectFile): Promise<boolean>
    abstract saveVideo(file: UploadProjectFile): Promise<boolean>
    abstract saveImageGroup(file: UploadProjectFile): Promise<boolean>
    abstract saveAudio(file: UploadProjectFile): Promise<boolean>

    private async saveModel3d(): Promise<void> {
        for (const file of this.files) {
            switch (file.type) {
                case FileType.audio:
                    await this.saveAudio(file)
                    break
                case FileType.img:
                    await this.saveImageGroup(file)
                    break
                case FileType.logo:
                    await this.saveLogo(file)
                    break
                case FileType.video:
                    await this.saveVideo(file)
                    break
                default:
            }
        }
    }
    private async saveGallery(): Promise<void> {
        for (const file of this.files) {
            switch (file.type) {
                case FileType.audio:
                    await this.saveAudio(file)
                    break
                case FileType.img:
                    await this.saveImageGroup(file)
                    break
                case FileType.logo:
                    await this.saveLogo(file)
                    break
                case FileType.video:
                    await this.saveVideo(file)
                    break
                default:
                    throw new Error('Unknown file type')
            }
        }
    }

    private async saveDeg360(): Promise<void> {
        for (const file of this.files) {
            switch (file.type) {
                case FileType.audio:
                    await this.saveAudio(file)
                    break
                case FileType.img:
                    await this.saveImage(file)
                    break
                case FileType.logo:
                    await this.saveLogo(file)
                    break
                case FileType.video:
                    await this.saveVideo(file)
                    break
                default:
                    throw new Error('Unknown file type')
            }
        }
    }

    private setGroupSettings() {
        this.files = this.files.map((f) => ({
            ...f,
            group: this.project.settings?.groupFiles.length,
        }))
    }

    async save(
        files: fileUpload.FileArray,
        type: FileType,
        saveOpt: any
    ): Promise<void> {
        this.files = this.extractFiles(files, type, saveOpt)

        switch (this.project.type) {
            case ProjectType.deg360:
                await this.saveDeg360()
                break
            case ProjectType.model3d:
                this.setGroupSettings()
                await this.saveModel3d()
                break
            case ProjectType.gallery:
                this.setGroupSettings()
                await this.saveGallery()
                break
            default:
                throw new Error('Unknown project type')
        }
    }

    private extractFiles(
        files: fileUpload.FileArray,
        type: FileType,
        saveOpt: any = {}
    ): UploadProjectFile[] {
        return Object.values(files).flatMap(
            (file: UploadedFile | UploadedFile[]) => {
                if (_.isArray(file))
                    return file.map((f) => {
                        const ext = f.name.match(/\.([^\.]+)$/)
                        return {
                            name: f.name,
                            buffer: f.data,
                            type,
                            mime: f.mimetype,
                            ext: ext ? ext[1] : null,
                        }
                    })

                const ext = file.name.match(/\.([^\.]+)$/)
                return {
                    name: file.name,
                    buffer: file.data,
                    type,
                    mime: file.mimetype,
                    ext: ext ? ext[1] : null,
                }
            }
        )
    }
}
