import { FileType, IProjectSchema, ProjectStorage } from '@root/src/interface'
import Cleaner from './Cleaner'
import LocalCleaner from './LocalCleaner'

export default class CleanerFactory {
    static create(
        project: IProjectSchema,
        inx: string,
        type: FileType
    ): Cleaner {
        switch (project.storage) {
            case ProjectStorage.local:
                return new LocalCleaner(project, inx, type)
            default:
                throw new Error('Unknown storage type')
        }
    }
}
