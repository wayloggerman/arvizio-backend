import {
    FileType,
    IProjectSchema,
    ProjectFile,
    ProjectType,
} from '@root/src/interface'

export default abstract class Cleaner {
    constructor(
        protected project: IProjectSchema,
        protected path: string,
        protected type: FileType
    ) {}
    abstract cleanLogo(): Promise<boolean>
    abstract cleanImage(): Promise<boolean>
    abstract cleanVideo(): Promise<boolean>
    abstract cleanImageGroup(): Promise<boolean>
    abstract cleanAudio(): Promise<boolean>

    async clean() {
        switch (this.project.type) {
            case ProjectType.deg360:
                return this.cleanDeg360()
            case ProjectType.model3d:
                return this.cleanModel3d()
            case ProjectType.gallery:
                return this.cleanGallery()
            default:
                throw new Error('Unknown project type')
        }
    }

    private async cleanModel3d(): Promise<void> {
        switch (this.type) {
            case FileType.audio:
                await this.cleanAudio()
                break
            case FileType.img:
                await this.cleanImageGroup()
                break
            case FileType.logo:
                await this.cleanLogo()
                break
            case FileType.video:
                await this.cleanVideo()
                break
            default:
        }
    }
    private async cleanGallery(): Promise<void> {
        switch (this.type) {
            case FileType.audio:
                await this.cleanAudio()
                break
            case FileType.img:
                await this.cleanImageGroup()
                break
            case FileType.logo:
                await this.cleanLogo()
                break
            case FileType.video:
                await this.cleanVideo()
                break
            default:
                throw new Error('Unknown file type')
        }
    }
    private async cleanDeg360(): Promise<void> {
        switch (this.type) {
            case FileType.audio:
                await this.cleanAudio()
                break
            case FileType.img:
                await this.cleanImage()
                break
            case FileType.logo:
                await this.cleanLogo()
                break
        }
    }
}
