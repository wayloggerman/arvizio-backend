import { FileType, IProjectSchema } from '@root/src/interface'
import Cleaner from './Cleaner'
import fs from 'fs/promises'
import cfg from '@root/cfg'
import Project from '../../Project'

export default class LocalCleaner extends Cleaner {
    constructor(
        protected project: IProjectSchema,
        protected path: string,
        protected type: FileType
    ) {
        super(project, path, type)
    }

    async cleanLogo(): Promise<boolean> {
        await fs.unlink(`${cfg.uploadsDir}/${this.path}`)
        this.project.logo_link = ''

        if (this.project.id && this.project.owner_id)
            await Project.update(
                this.project.id,
                this.project.owner_id,
                this.project
            )

        return true
    }
    async cleanImage(): Promise<boolean> {
        // await fs.unlink(`${cfg.uploadsDir}/${this.path}`)

        // const name = this.path.split('/').pop()

        // if (name && this.project.settings) {
        //     this.project.settings.files = this.project.settings.files.filter(
        //         (x) => x.name !== name
        //     )

        //     this.project.settings.files.forEach((file) => {
        //         file.spots = file.spots.filter(
        //             (x) => x.to !== name.split('.')[0]
        //         )
        //     })
        // }

        // if (this.project.id && this.project.owner_id)
        //     await Project.update(
        //         this.project.id,
        //         this.project.owner_id,
        //         this.project
        //     )

        return true
    }
    async cleanVideo(): Promise<boolean> {
        return true
    }
    async cleanImageGroup(): Promise<boolean> {
        try {
            // await fs.unlink(`${cfg.uploadsDir}/${this.path}`)
            const inx = this.path
            const group = this.project.settings.groupFiles[inx]

            this.project.settings.groupFiles.splice(parseInt(inx), 1)
            const { files } = group
            for (const file of files) {
                await fs.unlink(`${cfg.uploadsDir}/${file.path}`)
            }

            Project.update(this.project.id, this.project.owner_id, this.project)

            return true
        } catch (e) {
            console.log(e)
        }
    }
    async cleanAudio(): Promise<boolean> {
        return true
    }
}
