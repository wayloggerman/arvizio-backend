import { IProjectSchema, ProjectStorage } from '@root/src/interface';
import Loader from './Loader';
import LocalLoader from './LocalLoader';


export default class LoaderFactory {
    static create(project: IProjectSchema): Loader {
        switch (project.storage) {
            case ProjectStorage.local:
                return new LocalLoader(project);
            default:
                throw new Error('Unknown storage type');
        }
    }
}
