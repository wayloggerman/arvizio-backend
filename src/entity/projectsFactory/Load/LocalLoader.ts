import cfg from '@root/cfg'
import { IProjectSchema } from '@root/src/interface'
import fs from 'fs/promises'
import _ from 'lodash'
import Loader from './Loader'

export default class LocalLoader extends Loader {
    private images(path: string) {
        return this.project.settings?.files.find((x) => x.path === path)
    }

    private groupImages(path: string) {
        const groups = this.project.settings?.groupFiles
        if (!groups) return undefined

        const groupInx = parseInt(path.split('/')[2])

        if (_.isNil(groupInx)) return undefined

        const group = groups.find((x) => x.count === groupInx)
        if (!group) return undefined

        const image = group.files.find((x) => x.path === path)
        if (!image) return undefined
        return image
    }
    protected async loadfile(
        path: string,
        group: boolean
    ): Promise<Buffer | undefined | string> {
        try {
            const filePath = `${cfg.uploadsDir}/${path}`
            const image = group ? this.groupImages(path) : this.images(path)

            if (image) {
                return fs
                    .readFile(filePath, { encoding: 'base64' })
                    .then((data) => `data:${image.mime};base64,${data}`)
            }
            return undefined
        } catch (e) {
            return undefined
        }
    }

    constructor(protected project: IProjectSchema) {
        super(project)
    }
}
