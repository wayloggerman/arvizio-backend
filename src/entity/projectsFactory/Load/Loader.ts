import { FileType, IProjectSchema, ProjectType } from '@root/src/interface'

export default abstract class Loader {
    protected abstract loadfile(
        path: string,
        group: boolean
    ): Promise<Buffer | undefined | string>
    private async loadDeg360(
        path: string
    ): Promise<Buffer | undefined | string> {
        return this.loadfile(path, false)
    }

    private async loadModel3d(
        path: string
    ): Promise<Buffer | undefined | string> {
        return this.loadfile(path, true)
    }
    private async loadGallery(
        path: string
    ): Promise<Buffer | undefined | string> {
        return this.loadfile(path, true)
    }

    async load(path: string) {
        switch (this.project.type) {
            case ProjectType.deg360:
                return this.loadDeg360(path)
            case ProjectType.model3d:
                return this.loadModel3d(path)
            case ProjectType.gallery:
                return this.loadGallery(path)
            default:
                throw new Error('Unknown project type')
        }
    }

    constructor(protected project: IProjectSchema) {}
}
