import cfg from '@root/cfg'
import { Request, Response } from 'express'
import nanoid from 'nanoid'
import db from '../db/db'




export default class Session {


    static table = 'sessions'
    cols: {
        session_id: string,
        user_id: number
    } = { session_id: '', user_id: 0 }

    private constructor(user_id?: number, token?: string) {

        if (token) this.cols.session_id = token
        if (user_id) this.cols.user_id = user_id

    }

    static tokenGenerator() {
        const tokenChars = process.env.TOKEN_CHARS || 'abcdefijrlmnoqrstuvwxyz1234567890'

        const generator = nanoid.customAlphabet(tokenChars)

        return generator(20)
    }

    private async create() {

        const authToken = Session.tokenGenerator()

        return db(Session.table).insert({
            session_id: authToken,
            user_id: this.cols.user_id
        }).returning('session_id').then(res => res[0].session_id)


    }

    static create(user_id: number) {
        return new Session(user_id).create()
    }

    private async find() {
        return db(Session.table).select('user_id').where({ session_id: this.cols.session_id })
    }

    private async findAll() {

        return db(Session.table).select('session_id').where({
            user_id: db.raw('(select user_id from sessions where session_id=?)', [this.cols.session_id])
        })
    }

    static findAll(token: string) {
        return new Session(undefined, token).findAll()
    }

    static find(token: string) {
        return new Session(undefined, token).find()
    }





    private async destroy() {
        return db(Session.table).update({ is_drop: true }).where({ session_id: this.cols.session_id })
    }
    static destroy(token: string) {
        return new Session(undefined, token).destroy()
    }
}
