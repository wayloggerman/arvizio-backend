import cfg from '@root/cfg'
import delay from 'delay'
import { response, Router } from 'express'
import fileUpload, { UploadedFile } from 'express-fileupload'
import _ from 'lodash'
import db from '../db/db'
import Project from '../entity/Project'
import CleanerFactory from '../entity/projectsFactory/Cleaner/factory'
import LoaderFactory from '../entity/projectsFactory/Load/factory'
import save from '../entity/projectsFactory/Save/factory'
import Saver from '../entity/projectsFactory/Save/Saver'
import Session from '../entity/Session'
import {
    FileType,
    IProjectSchema,
    projectColumns,
    ProjectFile,
    ProjectType,
} from '../interface'
import { RouterModel } from './model'
import RouterTools from './tools'

export default class Projects extends RouterModel {
    static router = {
        path: '/projects',
        handler: () => new Projects().router(),
    }

    protected router(): Router {
        const router = Router()

        router.post('/create', async (request, response) => {
            await delay(cfg.apiDelay)

            const { name, parentSite, type, backLinkToParentSite, option } =
                request.body
            const { token } = request.cookies

            if (!token) {
                return RouterTools.res401(response)
            }

            if (!Object.values(ProjectType).includes(type)) {
                return response
                    .status(400)
                    .json({ error: 'Invalid project type' })
            }

            if ([name, type].includes(undefined)) {
                return response
                    .status(400)
                    .json({ error: 'Invalid project name or type' })
            }

            const res = await Project.create({
                name,
                parentSite,
                parentSiteText: backLinkToParentSite,
                type,
                token,
                option,
            }).then((res) => res[0].id)

            const userId = await db(Session.table)
                .select('user_id')
                .where({ session_id: token })
                .first()
                .then((res) => res.user_id)

            await Project.update(res, userId, {
                [projectColumns.link_to_change]: `${userId}/${res}`,
            })

            if (res) return RouterTools.res200(response, res)

            return RouterTools.res500(response)
        })

        router.post('/update', async (request, response) => {
            try {
                await delay(cfg.apiDelay)

                const { project } = request.body
                const { token } = request.cookies

                if (!token) {
                    return RouterTools.res401(response)
                }

                if (!project) {
                    return response
                        .status(400)
                        .json({ error: 'Invalid project' })
                }

                const userId = await db(Session.table)
                    .select('user_id')
                    .where({ session_id: token })
                    .first()
                    .then((res) => res.user_id)
                const res = await Project.update(project.id, userId, project)

                if (res) return RouterTools.res200(response, res)
            } catch (e) {
                return RouterTools.res500(response)
            }
        })

        router.get('/', async (request, response) => {
            console.log('get projects')

            await delay(cfg.apiDelay)
            const { token } = request.cookies

            const userId = await Session.find(token).then(
                (res) => res[0].user_id
            )

            const allProjects = await Project.getAll(userId).then((res) => res)

            return RouterTools.res200(response, allProjects)
        })

        router.get('/:id', async (request, response) => {
            await delay(cfg.apiDelay)
            const { id } = request.params

            const { token } = request.cookies

            const userId = await Session.find(token).then(
                (res) => res[0].user_id
            )

            const numId = typeof id === 'string' ? parseInt(id) : id

            const project: IProjectSchema = await Project.get(
                numId,
                userId
            ).then((res) => res)

            if (project.owner_id !== userId) {
                console.log({
                    owner: project.owner_id,
                    user: userId,
                })
                return RouterTools.res401(response)
            }

            if (project) return RouterTools.res200(response, project)

            return RouterTools.res404(response, 'Project not found')
        })

        router.put('/logo/', async (request, response) => {
            await delay(cfg.apiDelay)
            const { id } = request.body
            const { token } = request.cookies

            const files = request.files

            const userId = await Session.find(token).then(
                (res) => res[0].user_id
            )

            if ([files, id].includes(undefined))
                return RouterTools.res400(response, 'Bad request')

            if (!files) return RouterTools.res400(response, 'No files')

            const project = await Project.get(id, userId).then((res) => res)

            await save(project, files, FileType.logo)

            return RouterTools.res204(response)
        })

        router.put('/images/', async (request, response) => {
            await delay(cfg.apiDelay)
            const { id } = request.body
            const { token } = request.cookies
            const { group } = request.query

            const files = request.files

            if (!token) {
                return RouterTools.res401(response)
            }

            if ([files, id].includes(undefined))
                return RouterTools.res400(response, 'Bad request')

            if (!files) return RouterTools.res400(response, 'No files')

            const userId = await Session.find(token).then(
                (res) => res[0].user_id
            )

            let numId = typeof id === 'string' ? parseInt(id) : id

            const project = await Project.get(numId, userId).then((res) => res)

            await save(project, files, FileType.img, { group })

            return RouterTools.res204(response)
        })

        router.put('/audio/', async (request, response) => {
            await delay(cfg.apiDelay)
            const { id } = request.body
            const { token } = request.cookies
            const { group } = request.query

            const files = request.files

            if (!token) {
                return RouterTools.res401(response)
            }

            if ([files, id].includes(undefined))
                return RouterTools.res400(response, 'Bad request')

            if (!files) return RouterTools.res400(response, 'No files')

            const userId = await Session.find(token).then(
                (res) => res[0].user_id
            )

            const project = await Project.get(id, userId).then((res) => res)

            await save(project, files, FileType.audio, { group })

            return RouterTools.res204(response)
        })

        router.get('/:id/images', async (request, response) => {
            await delay(cfg.apiDelay)
            const { id } = request.params

            const { token } = request.cookies

            if (!token) {
                return RouterTools.res401(response)
            }

            const userId = await Session.find(token).then(
                (res) => res[0].user_id
            )
            const project = await Project.get(parseInt(id, 10), userId).then(
                (res) => res
            )

            if (project.owner_id !== userId) return RouterTools.res403(response)

            const images = await LoaderFactory.create(project).load(
                FileType.img
            )

            return RouterTools.res200(response, images)
        })
        router.post('/deleteImage', async (request, response) => {
            try {
                await delay(cfg.apiDelay)
                const { projectId, inx } = request.body

                console.log(request.body)

                if (!projectId || _.isNil(inx)) {
                    return RouterTools.res400(
                        response,
                        'Bad request: no projectId or numOfImage'
                    )
                }

                const { token } = request.cookies

                if (!token) {
                    return RouterTools.res401(response)
                }

                const userId = await Session.find(token).then(
                    (res) => res[0].user_id
                )
                const project = await Project.get(
                    parseInt(projectId, 10),
                    userId
                ).then((res) => res)

                if (project.owner_id !== userId)
                    return RouterTools.res403(response)

                await CleanerFactory.create(project, inx, FileType.img).clean()

                return RouterTools.res204(response)
            } catch (e) {
                console.log(e)

                return RouterTools.res404(response, 'Not found')
            }
        })

        router.delete('/:id', async (request, response) => {
            await delay(cfg.apiDelay)
            const { id } = request.params
            const { token } = request.cookies

            const userId = await Session.find(token).then(
                (res) => res[0].user_id
            )

            const updated = await Project.update(parseInt(id), userId, {
                [projectColumns.deleted]: true,
                [projectColumns.deletedAt]: new Date(),
            })
            if (updated) return RouterTools.res200(response, updated)

            return RouterTools.res404(response, 'Project not found')
        })

        return router
    }
}
