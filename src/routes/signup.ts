
import { Router, Request, Response, request } from 'express'

import nanoid from 'nanoid';
import db from '@root/src/db/db';

import RouterTools from './tools';
import Mailer from '@root/utils/mailer';
import confirmMail from '@root/public/mail/confirmSignup'
import { RouterModel } from './model';
import cfg from '@root/cfg';
import Session from '../entity/Session';
import User from '../entity/User';
import delay from 'delay';

export default class Signup extends RouterModel {


    protected router(): Router {
        const branch: Router = Router()
        branch.post('/', async (request: Request, response: Response) => {

            await delay(cfg.apiDelay)

            const { email, password } = request.body
            if ([email, password].includes(undefined)) {
                return RouterTools.res400(response, 'expect email, password')
            }

            // handle colysions
            const exist = await User.findByMail(email)

            if (exist) {
                return RouterTools.res400(response, 'email exist')
            }

            const confirmToken = Session.tokenGenerator()

            await User.create(email, password, confirmToken)

            await Mailer.sendMail(email, 'Registration on arvizio.com', '', confirmMail(email, confirmToken))

            return response.sendStatus(204)

        })


        branch.get('/confirm', async (request: Request, response: Response) => {

            await delay(cfg.apiDelay)
            const { token } = request.query

            if (!token) {
                return RouterTools.res400(response, 'expect token')
            }

            const resOfConfirm = await User.confirmUser(token as string)

            if (resOfConfirm.length <= 0) {
                return RouterTools.res404(response, 'already confirm or not found')
            }

            const newToken = await Session.create(resOfConfirm[0].id)

            response.cookie('token', newToken, cfg.cookieOpt)


            return RouterTools.redirect(response, `${cfg.url.frontend}`)


        })

        branch.get('/check', async (request: Request, response: Response) => {

            await delay(cfg.apiDelay)
            const { email } = request.query

            const res = await User.findByMail(email as string)

            if (!res) return RouterTools.res204(response)

            return RouterTools.res400(response, 'user exist')
        })
        return branch
    }


    constructor() {
        super()
    }


    static router = {
        path: '/signup',
        handler: new Signup().router
    }

}
