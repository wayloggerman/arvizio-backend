import { Router, Request, Response } from 'express';
import db from '../db/db';
import Session from '../entity/Session';
import { RouterModel } from './model';
import RouterTools from './tools';

export default class Auth extends RouterModel {

    protected router(): Router {
        const branch = Router()

        branch.get('/', async (request: Request, response: Response) => {

            const { token } = request.cookies


            if (!token) return RouterTools.res404(response, 'token not found')


            try {
                const res = await Session.find(token)

                if (!res.length)
                    return RouterTools.res404(response, 'session not found')

                console.log(token);


                return RouterTools.res200(response, {
                    token
                })
            }
            catch (e) {
                console.log({ e });

                return RouterTools.res500(response)
            }


        })

        return branch
    }

    constructor() {
        super()
    }


    static router = {
        path: '/auth',
        handler: () => new Auth().router()
    }
}
