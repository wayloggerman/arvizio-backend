import cfg from '@root/cfg'
import delay from 'delay'
import { Router } from 'express'
import Project from '../entity/Project'
import LoaderFactory from '../entity/projectsFactory/Load/factory'
import Session from '../entity/Session'
import { RouterModel } from './model'
import RouterTools from './tools'

export default class DataLoader extends RouterModel {
    static router = {
        path: '/data',
        handler: () => new DataLoader().router(),
    }

    private async load(req, res) {
        try {
            // await delay(cfg.apiDelay)

            const token = req.cookies.token
            const { userId, projectId, name, group } = req.params

            if (!token) {
                RouterTools.res401(res)
            }

            const numPid = parseInt(projectId)
            const numUid = parseInt(userId)

            const project = await Project.get(numPid, numUid)

            const loader = LoaderFactory.create(project)

            const url = group
                ? `${userId}/${projectId}/${group}/${name}`
                : `${userId}/${projectId}/${name}`
            const data = await loader.load(url)

            if (!data) {
                RouterTools.res404(res, 'File not found')
            }

            return RouterTools.res200raw(res, data)
        } catch (e) {
            console.log(e)

            RouterTools.res500(res)
        }
    }

    protected router(): Router {
        const router: Router = Router()

        router.get('/:userId/:projectId/:name', async (req, res) => {
            return this.load(req, res)
        })

        router.get('/:userId/:projectId/:group/:name', async (req, res) => {
            return this.load(req, res)
        })

        return router
    }
}
