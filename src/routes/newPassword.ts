import Mailer from '@root/utils/mailer';
import { Request, Response, Router } from 'express';
import db from '../db/db';
import { RouterModel } from './model';
import RouterTools from './tools';
import newPasswordMail from '@root/public/mail/newPassword';
import Session from '../entity/Session';
import User from '../entity/User';
import delay from 'delay';
import cfg from '@root/cfg';







export default class newPassword extends RouterModel {

    protected router(): Router {
        const router = Router()

        router.post('/', async (request: Request, response: Response) => {

            await delay(cfg.apiDelay)
            const { email } = request.body

            if (!email) return RouterTools.res400(response, 'expect email')

            const newPassword = Session.tokenGenerator()

            // {"email":"wwww@ww.ww","password":"no1xs6rq78xevd3ixrry"} 

            const res = await User.updatePassword(email, newPassword)


            if (!res) return RouterTools.res404(response, 'email not found')


            const newPass = await Mailer.sendMail(email, 'arvizio.com: newPassword', '', newPasswordMail(email, newPassword))


            return RouterTools.res204(response)

        })


        return router
    }


    static router = {
        path: '/newPassword',
        handler: () => new newPassword().router()
    }
}
