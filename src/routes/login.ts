import cfg from '@root/cfg';
import delay from 'delay';
import { Router, Request, Response } from 'express';
import db from '../db/db';
import Session from '../entity/Session';
import User from '../entity/User';
import { RouterModel } from './model';
import RouterTools from './tools';



export default class Login extends RouterModel {

    protected router(): Router {
        const branch = Router()

        branch.post('/', async (request: Request, response: Response) => {

            await delay(cfg.apiDelay)
            const { email, password } = request.body



            if ([email, password].includes(undefined)) return RouterTools.res400(response, 'expect email,password')


            try {
                const res = await User.find(email, password)

                if (!res.length) {
                    return RouterTools.res401(response)
                }

                if (!res[0].is_confirmed) {
                    return RouterTools.res403(response)
                }

                const userId = res[0].id

                const newToken = await Session.create(userId)

                response.cookie('token', newToken, cfg.cookieOpt)


                return RouterTools.res200(response, newToken
                )
            }
            catch (e) {

                return RouterTools.res500(response)
            }


        })

        return branch
    }

    constructor() {
        super()
    }


    static router = {
        path: '/login',
        handler: () => new Login().router()
    }
}
