import cfg from '@root/cfg';
import { Request, Response } from 'express';



export default class RouterTools {

    static res200raw(response: Response, data: any) {
        return response.status(200).send(data)
    }

    static res400(response: Response, reason: string) {
        return response.status(400).json({
            reason
        })
    }

    static res404(response: Response, reason: string) {
        return response.status(404).json({
            reason
        })
    }

    static res204(response: Response) {
        return response.sendStatus(204)
    }
    static res200(response: Response, data: any) {
        return response.status(200).json({
            data
        })
    }

    static res500(response: Response) {
        return response.sendStatus(500)
    }

    static res401(response: Response) {
        return response.sendStatus(401)
    }
    static res403(response: Response) {
        return response.sendStatus(403)
    }

    static redirect(response: Response, url: string) {
        if (cfg.url.frontend)
            return response.redirect(url)
    }
}
