import cfg from '@root/cfg';
import delay from 'delay';
import { Request, Response, Router } from 'express';
import db from '../db/db';
import Session from '../entity/Session';
import { RouterModel } from './model';
import RouterTools from './tools';




export default class Logout extends RouterModel {


    protected router(): Router {
        const router: Router = Router()

        router.delete('/', async (request: Request, response: Response) => {

            await delay(cfg.apiDelay)
            try {
                const { token } = request.cookies


                if (!token) return RouterTools.res404(response, 'session not found')

                const session = await Session.destroy(token)


                response.clearCookie('token')
                if (!session) {
                    return RouterTools.res404(response, "session not found")
                }

                return RouterTools.res204(response)

            }
            catch (e) {
                console.log(e);

                return RouterTools.res500(response)
            }




        })


        return router
    }

    static router = {
        path: '/logout',
        handler: () => new Logout().router()
    }
}
