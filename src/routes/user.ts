import { response, Router } from 'express';
import Session from '../entity/Session';
import User from '../entity/User';
import { RouterModel } from './model';
import RouterTools from './tools';



export default class UserRoute extends RouterModel {
    protected router(): Router {
        const router: Router = Router()

        router.get('/', async (request, response) => {

            try {

                const { token } = request.cookies

                if (!token) return RouterTools.res400(response, 'expoect token');

                const userId = await Session.find(token).then(res => res[0].user_id)

                if (!userId)
                    return RouterTools.res400(response, 'cant find user')

                const user = await User.findById(userId)

                return RouterTools.res200(response, user[0])
            }
            catch (e) {
                return RouterTools.res500(response)
            }




        })

        return router
    }


    static router = {
        path: '/user',
        handler: () => new UserRoute().router()
    }
}
