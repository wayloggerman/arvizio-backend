import { Router } from 'express';



export abstract class RouterModel {
    protected abstract router(): Router
}
