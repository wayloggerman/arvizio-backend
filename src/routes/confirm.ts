import confirmMail from '@root/public/mail/confirmSignup';
import Mailer from '@root/utils/mailer';
import { Request, Response, Router } from 'express';
import db from '../db/db';
import Session from '../entity/Session';
import User from '../entity/User';
import { RouterModel } from './model';
import RouterTools from './tools';




export default class Confirm extends RouterModel {


    protected router(): Router {
        const router = Router()

        router.post('/', async (request: Request, response: Response) => {
            try {
                const { email } = request.body

                if (!email) {
                    return RouterTools.res400(response, 'expect email')
                }

                const token = Session.tokenGenerator()

                const res = await User.refreshConfirm(email, token)

                if (!res) {
                    return RouterTools.res404(response, 'not found email')
                }

                //send mail

                await Mailer.sendMail(email, 'Registration on arvizio.com', '', confirmMail(email, token))




                return RouterTools.res204(response)



            }
            catch (e) {

                return RouterTools.res500(response)
            }


        })

        return router
    }


    static router = {
        path: '/confirm',
        handler: () => new Confirm().router()
    }
}
