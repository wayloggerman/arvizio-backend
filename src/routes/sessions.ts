import cfg from '@root/cfg';
import delay from 'delay';
import { Router } from 'express';
import Session from '../entity/Session';
import { RouterModel } from './model';
import RouterTools from './tools';


export default class Sessions extends RouterModel {
    static router = {
        path: '/getSessions',
        handler: () => new Sessions().router()
    }
    protected router(): Router {
        const router: Router = Router()

        router.get('/', async (req, res) => {

            await delay(cfg.apiDelay)

            const token = req.cookies.token

            if (!token) {
                RouterTools.res401(res)
                return
            }


            const sessions = await Session.findAll(token)

            RouterTools.res200(res, sessions)


        })

        return router
    }

}
