import { NextFunction, Request, Response } from 'express';
import db from '../db/db';
import RouterTools from '../routes/tools';




export default function () {

    return async (request: Request, response: Response, next: NextFunction) => {



        const { token } = request.cookies

        if (!token)
            return RouterTools.res401(response)

        const res = await db('sessions').select('user_id').where({ session_id: token })

        if (!res.length)
            return RouterTools.res401(response)


        next()
    }

}
