export enum ProjectType {
    deg360 = 'deg360',
    model3d = 'model3d',
    gallery = 'gallery',
}

export enum FileType {
    img = 'img',
    video = 'video',
    logo = 'logo',
    audio = 'audio',
}

export enum ProjectStorage {
    local = 'local',
    sftp = 'sftp',
    ftp = 'ftp',
    smb = 'smb',
    webdav = 'webdav',
    s3 = 's3',
    git = 'git',
    github = 'github',
    gitlab = 'gitlab',
    bitbucket = 'bitbucket',
    dropbox = 'dropbox',
}

export interface UploadProjectFile {
    name: string
    type: FileType
    mime: string
    buffer: Buffer
    ext: string | null
    group?: number | undefined
}

export interface Spot {
    from: string
    to: string
    vector: any
}

export interface ProjectFile {
    name: string
    path: string
    type: FileType
    mime: string
    spots: Spot[]
}

export interface GroupFiles {
    groupKey: string
    count: number
    files: ProjectFile[]
}
export interface ProjectSettings {
    parentSite: string
    textParentSite: string
    deleted: boolean
    deletedAt: string
    files: ProjectFile[]
    imageCounter: number
    groupFiles: GroupFiles[]
}

export const defaultSettings: ProjectSettings = {
    parentSite: '',
    textParentSite: '',
    deleted: false,
    deletedAt: '',
    files: [],
    groupFiles: [],
    imageCounter: 0,
}

export interface IProjectSchema {
    id?: number
    name?: string
    type?: ProjectType
    owner_id?: number
    size_in_bytes?: string
    link_to_read?: string
    link_to_change?: string
    free_to_change?: boolean
    created_at?: Date
    updated_at?: Date
    storage?: ProjectStorage
    settings?: ProjectSettings
    deleted?: boolean
    deleted_at?: Date
    logo_link?: string
}

export enum projectColumns {
    id = 'id',
    name = 'name',
    type = 'type',
    owner_id = 'owner_id',
    size_in_bytes = 'size_in_bytes',
    link_to_read = 'link_to_read',
    link_to_change = 'link_to_change',
    free_to_change = 'free_to_change',
    created_at = 'created_at',
    updated_at = 'updated_at',
    storage = 'storage',
    settings = 'settings',
    deleted = 'deleted',
    deletedAt = 'deleted_at',
    logo_link = 'logo_link',
}
