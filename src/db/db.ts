import knex from 'knex';



const mode = process.env.NODE_ENV === 'production' ? 'production' : 'development'


export default knex(
    mode === 'development' ?
        {
            client: 'pg',
            connection: {
                database: process.env.TEST_DB,
                user: process.env.TEST_DB_USER,
                password: process.env.TEST_DB_PASSWORD,
                host: process.env.TEST_DB_HOST,
                port: parseInt(process.env.TEST_DB_PORT ? process.env.TEST_DB_PORT : '5432', 10),
            },
            pool: {
                min: 2,
                max: 10
            }

        } : {
            client: 'pg',
            connection: {
                database: process.env.PROD_DB,
                user: process.env.PROD_DB_USER,
                password: process.env.PROD_DB_PASSWORD,
                host: process.env.PROD_DB_HOST,
                port: parseInt(process.env.PROD_DB_PORT ? process.env.PROD_DB_PORT : '5432', 10),
            },
            pool: {
                min: 2,
                max: 10
            }
        })
