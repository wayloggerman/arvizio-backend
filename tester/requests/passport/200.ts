import axios from 'axios';


export default async () => {
    console.log('auth/vk/200.ts');
    const url = process.env.VK
    console.log({ url });


    if (!url) return


    try {
        const res = await axios.get(url)

        console.log({
            request: {
                url,
            },
            response: {
                status: res.status,
                statusText: res.statusText,
                data: res.data,
            }
        });

    }
    catch (e) {
        console.log({
            request: {
                url,
            },
            error: e

        });

    }

}
