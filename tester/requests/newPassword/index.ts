


import res204 from './204'
import res400 from './400'
import res404 from './404'



(async () => {
    await res204()
    await res400()
    await res404()
})()
