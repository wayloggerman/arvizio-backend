import axios from 'axios';


export default async () => {
    console.log('newPassword/400.ts');
    const url = process.env.NEW_PASSWORD_URL

    if (!url) return


    try {
        const res = await axios.post(url, {
            emasil: 'wayloggssser@mail.ru'
        })

        console.log({
            request: {
                url,
            },
            response: {
                status: res.status,
                statusText: res.statusText,
                data: res.data,
            }
        });

    }
    catch (e) {
        console.log({
            request: {
                url,
            },
            error: e

        });

    }

}
