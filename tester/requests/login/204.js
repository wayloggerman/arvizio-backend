"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const res204 = () => __awaiter(void 0, void 0, void 0, function* () {
    console.log('login/200.ts');
    const url = process.env.LOGIN_URL;
    const data = {
        email: 'waylogger@mail.ru',
        password: '12345678'
    };
    console.log({ url, data });
    if (!url) {
        return;
    }
    try {
        const res = yield axios_1.default.post(url, data, { withCredentials: true });
        console.log({
            request: {
                url,
                data: JSON.stringify(data)
            },
            response: {
                status: res.status,
                statusText: res.statusText,
                cookie: res.headers['set-cookie']
            }
        });
    }
    catch (e) {
        console.log({
            request: {
                url,
                data: JSON.stringify(data)
            },
            status: e.response.status,
            data: e.response.data,
        });
    }
});
exports.default = res204;
