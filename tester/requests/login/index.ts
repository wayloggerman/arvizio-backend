

import res204 from './204'
import rand401 from './401'


(async () => {
    await rand401()
    await res204()

})()
