

import axios from 'axios'

const err400 = async () => {
    console.log('signup/400.ts');


    const url = process.env.SIGNUP_URL

    if (!url) return

    const data = {
    }
    try {
        const res = await axios.post(url, data)

        console.log({
            request: {
                url,
                data
            },
            response: {
                status: res.status,
                statusText: res.statusText,
                data: res.data,
            }
        });

    }
    catch (e) {
        console.log({
            request: {
                url,
                data
            },
            status: (e as any).response.status,
            data: (e as any).response.data,

        });

    }


}


export default err400



