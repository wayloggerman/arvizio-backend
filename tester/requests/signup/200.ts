

import axios from 'axios'

import nanoid from 'nanoid';

const rand200 = async () => {
    const generator = nanoid.customAlphabet('abcdefgijklmnopqrstuvwxyz01234567890_', 10)

    console.log('signup/200.ts');
    const url = process.env.SIGNUP_URL

    if (!url) return

    const data = {
        email: `${generator()}@mail.ru`,
        password: 'awdawfawd'
    }
    try {
        const res = await axios.post(url, data)

        console.log({
            request: {
                url,
                data
            },
            response: {
                status: res.status,
                statusText: res.statusText,
                data: res.data,
            }
        });

    }
    catch (e) {
        console.log({
            request: {
                url,
                data
            },
            error: e

        });

    }


}

export default rand200



