

import rand200 from './200'
import real200 from './200-real'
import err400 from './400'

(async () => {
    await real200()
    await rand200()
    await err400()
})()
