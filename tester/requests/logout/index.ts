

import res204 from './204'
import res404 from './404'

(async () => {
    await res204()
    await res404()
})()
