"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const res204 = () => __awaiter(void 0, void 0, void 0, function* () {
    console.log('logout/204.ts');
    const url = process.env.LOGOUT_URL;
    if (!url)
        return;
    try {
        const res = yield axios_1.default.delete(url, {
            headers: {
                Cookie: "token=Y-EP10UlUwHFfbA-VNxva"
            }
        });
        console.log({
            request: {
                url,
            },
            response: {
                status: res.status,
                statusText: res.statusText,
                data: res.data,
            }
        });
    }
    catch (e) {
        console.log({
            request: {
                url,
            },
            error: e
        });
    }
});
exports.default = res204;
