

import axios from 'axios'

const res204 = async () => {
    console.log('logout/204.ts');
    const url = process.env.LOGOUT_URL

    if (!url) return


    try {
        const res = await axios.delete(url, {
            headers: {
                Cookie: "token=Y-EP10UlUwHFfbA-VNxva"
            }
        })

        console.log({
            request: {
                url,
            },
            response: {
                status: res.status,
                statusText: res.statusText,
                data: res.data,
            }
        });

    }
    catch (e) {
        console.log({
            request: {
                url,
            },
            error: e

        });

    }


}

export default res204



