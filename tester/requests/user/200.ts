


import axios from 'axios'

export default async () => {


    console.log('user/200.ts');
    const url = process.env.USER_URL

    if (!url) return


    try {
        const res = await axios.get(url, {
            headers: {
                'cookie': "token=9l3j1t1ct5q8nics97lr"
            }
        })


        console.log({
            request: {
                url,
            },
            response: {
                status: res.status,
                statusText: res.statusText,
                data: JSON.stringify(res.data),
            }
        });

    }
    catch (e) {
        console.log({
            request: {
                url,
            },
            error: e

        });

    }



}
