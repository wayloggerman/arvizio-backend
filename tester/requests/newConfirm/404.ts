

import axios from 'axios'

export default async () => {
    console.log('newConfirm/204.ts');
    const url = process.env.NEW_CONFIRM_URL

    if (!url) return


    try {
        const res = await axios.post(url, {
            email: 'wayloggerman333443@mail.ru'
        })

        console.log({
            request: {
                url,
            },
            response: {
                status: res.status,
                statusText: res.statusText,
                data: res.data,
            }
        });

    }
    catch (e) {
        console.log({
            request: {
                url,
            },
            error: e

        });

    }


}




