import fs from 'fs'
import _ from 'lodash'


const mode = process.env.NODE_ENV === 'production' ? 'production' : 'development'

const IS_PROD = mode === 'production'

const IS_DEV = !IS_PROD

const confirmURL = IS_PROD ? process.env.PROD_CONFIRM_URL : process.env.TEST_CONFIRM_URL

const mailerUser = IS_PROD ? process.env.TEST_MAILING_USER : process.env.TEST_MAILING_USER
const mailerPass = IS_PROD ? process.env.TEST_MAILING_PASS : process.env.TEST_MAILING_PASS

const frontend = IS_PROD ? process.env.PROD_FRONTEND : process.env.TEST_FRONTEND

const uploadsDir = `/Users/waylogger/arvizio/uploads`
const staticUploadDir = 'uploads'

const ssl = {
    key: IS_PROD ? fs.readFileSync("/etc/letsencrypt/live/www.arviziotest.ru/privkey.pem") : '',
    cert: IS_PROD ? fs.readFileSync("/etc/letsencrypt/live/www.arviziotest.ru/fullchain.pem") : '',
    ca: IS_PROD ? [
        fs.readFileSync("/etc/letsencrypt/live/www.arviziotest.ru/cert.pem"),
        fs.readFileSync("/etc/letsencrypt/live/www.arviziotest.ru/chain.pem"),
    ] : []
}




export default {
    mode,
    url: {
        confirmURL,
        frontend
    },
    IS_DEV,
    IS_PROD,
    mailer: {
        mailerPass,
        mailerUser
    },
    cookieOpt: {
        maxAge: 1000 * 60 * 60 * 24 * 10,
        httpOnly: true,
        SameSite: 'None',
    },
    uploadsDir,
    staticUploadDir,
    apiDelay: 1,
    ssl,

}
