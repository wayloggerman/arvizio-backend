


import cfg from '@root/cfg';
import nodemailer from 'nodemailer'




export default class Mailer {


    private transport = nodemailer.createTransport({
        host: "smtp.yandex.ru",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: cfg.mailer.mailerUser,
            pass: cfg.mailer.mailerPass
        }
    });
    private constructor(private to: string, private subject: string, private text: string, private html: string) {

    }

    private async sendMail() {

        return this.transport.sendMail({
            from: cfg.mailer.mailerUser,
            to: this.to,
            subject: this.subject,
            text: this.text,
            html: this.html
        })

    }

    static sendMail(to: string, subject: string, text: string, html: string) {
        return new Mailer(to, subject, text, html).sendMail()
    }
}
