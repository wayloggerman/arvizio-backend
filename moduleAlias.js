const moduleAlias = require('module-alias')
moduleAlias.addAliases({
    '@root': __dirname,
    '@t': __dirname + '/types',
})
moduleAlias()
