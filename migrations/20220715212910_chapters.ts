import { Knex } from "knex";



const table = 'chapters_of_projects'
export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(table, table => {
        table.increments('id').primary()
        table.integer('project_id').references('id').inTable('projects')
        table.jsonb('settings')
        table.boolean('deleted').defaultTo(false)
        table.timestamp('deleted_at').defaultTo(knex.fn.now())
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(table)
}

