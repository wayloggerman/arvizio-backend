import { Knex } from "knex";

const table = 'projects'

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(table, table => {
        table.increments('id').primary()
        table.string('name').notNullable()
        table.string('type')
        table.integer('owner_id').references('id').inTable('users')
        table.string('size_in_bytes')
        table.string('link_to_read')
        table.string('link_to_change')
        table.boolean('free_to_change')
        table.timestamps(true, true)

    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(table)
}

