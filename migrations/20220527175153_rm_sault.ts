import { Knex } from 'knex'

const tableName = 'users'
const columnName = 'sault'

export async function up(knex: Knex): Promise<void> {

    return knex.schema.table(tableName, async (table) => {
        table.dropColumn(columnName)
    })
}

export async function down(knex: Knex): Promise<void> {

    return knex.schema.table(tableName, async (table) => {
        table.string(columnName)
    })
}
