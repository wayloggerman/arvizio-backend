import { Knex } from "knex";

const tableName = 'sessions'
export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(tableName, (table) => {
        table.string('session_id').primary()
        table.integer('user_id')
        table.foreign('user_id').references('id').inTable('users')

    })

}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(tableName)
}
