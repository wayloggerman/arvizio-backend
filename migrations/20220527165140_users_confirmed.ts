import { Knex } from 'knex'

const tableName = 'users'
const newColumnName = 'is_confirmed'

export async function up(knex: Knex): Promise<void> {

    return knex.schema.table(tableName, async (table) => {
        table.boolean(newColumnName).defaultTo(1)
    })
}

export async function down(knex: Knex): Promise<void> {

    return knex.schema.table(tableName, async (table) => {
        table.dropColumn(newColumnName)
    })
}
