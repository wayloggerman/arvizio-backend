import { Knex } from "knex";

const tableName = 'users';
export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary()
        table.string('email').unique()
        table.string('password')
        table.string('sault')
        table.timestamp('created_at')

    })

}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(tableName)
}
