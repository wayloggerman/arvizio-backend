import { Knex } from "knex";


const table = 'projects'
const col = 'logo_link'
export async function up(knex: Knex): Promise<void> {
    return knex.schema.table(table, (t) => {
        t.string(col, 128).defaultTo('')
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.table(table, (t) => {
        t.dropColumn(col)
    })
}

