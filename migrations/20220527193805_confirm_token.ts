import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

    return knex.schema.table('users', (table) => {
        return table.string('confirm_token')
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.table('users', (table) => {
        return table.dropColumn('confirm_token')
    })
}

