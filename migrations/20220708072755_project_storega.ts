import { Knex } from "knex";


const table = 'projects'
export async function up(knex: Knex): Promise<void> {
    return knex.schema.table(table, table => {
        table.string('storage', 2048)
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.table(table, table => {
        table.dropColumn('storage')
    })
}

