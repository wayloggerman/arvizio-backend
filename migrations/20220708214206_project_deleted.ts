import { Knex } from "knex";

const table = 'projects'
export async function up(knex: Knex): Promise<void> {
    return knex.schema.table(table, (t) => {
        t.boolean('deleted').defaultTo(false)
        t.timestamp('deleted_at')
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.table(table, (t) => {
        t.dropColumn('deleted')
        t.dropColumn('deleted_at')
    })

}


