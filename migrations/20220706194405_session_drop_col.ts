import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.table('sessions', (table) => {
        table.boolean('is_drop').defaultTo(false)
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.table('sessions', (table) => {
        table.dropColumn('is_drop')
    })
}

