import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.table('users', (table) => {
        return table.string('source_external_id').defaultTo('email|-')
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.table('users', (table) => {
        return table.dropColumn('source_external_id')
    })
}

