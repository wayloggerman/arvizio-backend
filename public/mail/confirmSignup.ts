import cfg from '@root/cfg'

const confirmMail = (email: string, token: string) => {
    return `<div>
  <h1>Hello ${email} !</h1>
  <h3>
    this address used for registration on site
    <a href="www.arvizio.com">www.arvizio.com</a> , for confirm registration
    please click here: <a href="${cfg.url.confirmURL}?token=${token}">CONFIRM</a>
  </h3>
</div> `

}

export default confirmMail
